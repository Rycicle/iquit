//
//  UIView+LayerEffects.m
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "UIView+LayerEffects.h"

@implementation UIView (LayerEffects)

-(void)setLayerEffect:(int)layerEffect
{
    if(layerEffect == 1){
        self.layer.cornerRadius = 5.0;
    }
}

@end
