//
//  UIColor+iQuitColors.h
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (iQuitColors)

+(UIColor*)iQuitGrey1;

@end
