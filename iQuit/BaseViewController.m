//
//  BaseViewController.m
//  iQuit
//
//  Created by Ryan Salton on 20/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController {
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.navigationBar = [[UIView alloc] initWithFrame:CGRectMake(0, -70, self.view.frame.size.width, 70)];
    self.navigationBar.backgroundColor = [UIColor redColor];
    self.navigationBarTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height - 20)];
    self.navigationBarTitle.textAlignment = NSTextAlignmentCenter;
    self.navigationBarTitle.font = [UIFont systemFontOfSize:24];
    self.navigationBarTitle.textColor = [UIColor whiteColor];
    self.navigationBarTitle.backgroundColor = [UIColor clearColor];
    
    self.navLeftButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 20, 50, 50)];
    self.navRightButton = [[UIButton alloc] initWithFrame:CGRectMake(self.navigationBar.frame.size.width - 70, 20, 60, 50)];
    self.navRightButton.titleLabel.textAlignment = NSTextAlignmentRight;
    
    [self.navRightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];

    [self.navigationBar addSubview:self.navigationBarTitle];
    [self.navigationBar addSubview:self.navLeftButton];
    [self.navigationBar addSubview:self.navRightButton];
    [self.view addSubview:self.navigationBar];
    
    //    self.navigationBar.layer.zPosition = 100;
    if(IOS_7_OR_ABOVE){
        [self setNeedsStatusBarAppearanceUpdate];
    }
}

-(void)showNavigationBarWithAnimation:(BOOL)animate
{
    if(animate){
        [UIView animateWithDuration:0.5 animations:^{
            self.navigationBar.frame = CGRectMake(self.navigationBar.frame.origin.x, 0, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height);
        }];
    }
    else{
        self.navigationBar.frame = CGRectMake(self.navigationBar.frame.origin.x, 0, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height);
    }
}

-(void)hideNavigationBarWithAnimation:(BOOL)animate
{
    if(animate){
        [UIView animateWithDuration:0.5 animations:^{
            self.navigationBar.frame = CGRectMake(self.navigationBar.frame.origin.x, -self.navigationBar.frame.size.height, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height);
        }];
    }
    else{
        self.navigationBar.frame = CGRectMake(self.navigationBar.frame.origin.x, -self.navigationBar.frame.size.height, self.navigationBar.frame.size.width, self.navigationBar.frame.size.height);
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
