//
//  UserModel.m
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "UserModel.h"
#import "AddictionModel.h"

@implementation UserModel

-(NSDictionary *)toDictionary
{
    NSDictionary *dict;
    
    NSMutableArray *addictions = [NSMutableArray array];
    for (AddictionModel *model in self.userAddictions) {
        [addictions addObject:[model toDictionary]];
    }
    
    dict = [[NSDictionary alloc] initWithObjects:@[addictions] forKeys:@[@"Addictions"]];
    
    return dict;
}

@end
