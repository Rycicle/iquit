//
//  UIColor+iQuitColors.m
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "UIColor+iQuitColors.h"

@implementation UIColor (iQuitColors)

+(UIColor *)iQuitGrey1
{
    return [UIColor colorWithRed:217.0/255.0 green:222.0/255.0 blue:231.0/255.0 alpha:1.0];
}

@end
