//
//  DataManager.h
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserModel.h"

@interface DataManager : NSObject

@property (nonatomic, strong) UserModel *userModel;

+(DataManager *)sharedInstance;
-(void)saveUserModel;
-(void)loadUserModel;

@end
