//
//  ViewController.m
//  iQuit
//
//  Created by Ryan Salton on 20/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "ViewController.h"
#import "AddictionsCell.h"
#import "SetupViewController.h"

@interface ViewController ()

@end

@implementation ViewController {
    UILabel *mainTitle;
    AddictionsCollectionView *addictionView;
    NSDictionary *addictionsDictionary;
    NSArray *dictKeys;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    mainTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, self.view.frame.size.width, 100)];
    mainTitle.text = @"iQuit";
    mainTitle.font = [UIFont systemFontOfSize:90];
    mainTitle.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:mainTitle];
    
    dictKeys = @[@"Smoking", @"Chocolate", @"Alcohol", @"Drugs", @"Nailbiting"];
    
    NSString *file = [[NSBundle mainBundle] pathForResource:@"topAddictions" ofType:@"plist"];
    addictionsDictionary = [[NSDictionary alloc] initWithContentsOfFile:file];
    
    UICollectionViewFlowLayout *layout = [UICollectionViewFlowLayout new];
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.itemSize = CGSizeMake(250, 250);
    layout.minimumInteritemSpacing = 4;
    layout.minimumLineSpacing = 4;
    layout.sectionInset = UIEdgeInsetsMake(0, (self.view.frame.size.width/2) - 125, 0, (self.view.frame.size.width/2) - 125);
    
    addictionView = [[AddictionsCollectionView alloc] initWithFrame:CGRectMake(0, mainTitle.frame.origin.y + mainTitle.frame.size.height + 46, self.view.frame.size.width, 250) collectionViewLayout:layout];
    [addictionView registerClass:[AddictionsCell class] forCellWithReuseIdentifier:@"Cell"];
    addictionView.delegate = self;
    addictionView.dataSource = self;
    [self.view addSubview:addictionView];
    
    UIButton *somethingElseButton = [[UIButton alloc] initWithFrame:CGRectMake(24, addictionView.frame.origin.y + addictionView.frame.size.height + 26, self.view.frame.size.width - 48, 45)];
    [somethingElseButton setTitle:@"I'm addicted to something else" forState:UIControlStateNormal];
    somethingElseButton.titleLabel.font = [UIFont systemFontOfSize:18];
    [somethingElseButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [somethingElseButton setBackgroundColor:[UIColor iQuitGrey1]];
    [somethingElseButton setLayerEffect:1];
    somethingElseButton.layer.borderWidth = 2.0;
    somethingElseButton.layer.borderColor = [UIColor colorWithRed:118.0/255.0 green:121.0/255.0 blue:125.0/255.0 alpha:1.0].CGColor;
    [self.view addSubview:somethingElseButton];
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return addictionsDictionary.count;
//    return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    AddictionsCell *cell = [addictionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    NSDictionary *cellDict = [addictionsDictionary objectForKey:dictKeys[indexPath.row]];
    
    cell.cellTitle.text = [cellDict objectForKey:@"Title"];
    cell.cellImage.image = [UIImage imageNamed:[cellDict objectForKey:@"Image"]];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    SetupViewController *viewController = [SetupViewController new];
    [self.navigationController pushViewController:viewController animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
