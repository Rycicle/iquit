//
//  SetupViewController.h
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "BaseViewController.h"

@interface SetupViewController : BaseViewController <UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>

@end
