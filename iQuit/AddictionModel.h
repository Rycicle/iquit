//
//  AddictionModel.h
//  iQuit
//
//  Created by Ryan Salton on 29/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AddictionModel : NSObject

@property (nonatomic, strong) NSNumber *currency;
@property (nonatomic, strong) NSNumber *costPerDay;

-(NSDictionary*)toDictionary;

@end
