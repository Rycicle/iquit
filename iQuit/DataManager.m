//
//  DataManager.m
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+ (DataManager *)sharedInstance
{
    static dispatch_once_t pred;
    static DataManager *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[self alloc] init];
    });
    
    return instance;
}

- (id)init
{
    self = [super init];
    
    if (self)
    {

    }
    
    return self;
}

-(void)saveUserModel
{
    NSDictionary *dict = [[NSDictionary alloc] initWithDictionary:[self.userModel toDictionary]];
    
    [[NSUserDefaults standardUserDefaults] setObject:dict forKey:@"UserModel"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

-(void)loadUserModel
{
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"UserModel"]){
        NSDictionary *dict = [[NSUserDefaults standardUserDefaults] objectForKey:@"UserModel"];
        
        UserModel *model = [UserModel new];
        model.userAddictions = [NSMutableArray array];
        
    //    for(){
    //        
    //    }
        
    //    self.userModel = ;
            
    }
        
}

@end
