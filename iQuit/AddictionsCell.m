//
//  AddictionsCell.m
//  iQuit
//
//  Created by Ryan Salton on 20/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "AddictionsCell.h"

@implementation AddictionsCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor greenColor];
        
        self.cellImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
        [self addSubview:self.cellImage];
        
        
        
        self.cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, frame.size.width, 50)];
        self.cellTitle.textAlignment = NSTextAlignmentCenter;
        
        UIView *titleBG = [[UIView alloc] initWithFrame:self.cellTitle.frame];
        titleBG.backgroundColor = [UIColor whiteColor];
        titleBG.alpha = 0.4;
        [self addSubview:titleBG];
        
        [self addSubview:self.cellTitle];
        
        
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
