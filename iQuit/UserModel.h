//
//  UserModel.h
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, strong) NSMutableArray *userAddictions;

-(NSDictionary*)toDictionary;

@end
