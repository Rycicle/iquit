//
//  AddictionModel.m
//  iQuit
//
//  Created by Ryan Salton on 29/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "AddictionModel.h"

@implementation AddictionModel

-(NSDictionary*)toDictionary
{
    NSDictionary *dict;
    
    dict = [[NSDictionary alloc] initWithObjects:@[self.currency, self.costPerDay] forKeys:@[@"currency", @"costPerDay"]];
    
    return dict;
}

@end
