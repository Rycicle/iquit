//
//  BaseViewController.h
//  iQuit
//
//  Created by Ryan Salton on 20/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController

@property (nonatomic, strong) UIView *navigationBar;
@property (nonatomic, strong) UILabel *navigationBarTitle;
@property (nonatomic, strong) UIButton *navLeftButton;
@property (nonatomic, strong) UIButton *navRightButton;

-(void)showNavigationBarWithAnimation:(BOOL)animate;
-(void)hideNavigationBarWithAnimation:(BOOL)animate;

@end
