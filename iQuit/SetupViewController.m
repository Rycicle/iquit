//
//  SetupViewController.m
//  iQuit
//
//  Created by Ryan Salton on 27/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import "SetupViewController.h"

@interface SetupViewController ()

@end

@implementation SetupViewController {
    UILabel *howMuchLabel;
    UITextField *moneyTextField;
    UIPickerView *timeSpanPicker;
    
    UIButton *poundButton;
    UIButton *dollarButton;
    NSInteger currencySelected;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.view.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self showNavigationBarWithAnimation:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationBarTitle.text = @"Setup";
    
    howMuchLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, self.navigationBar.frame.size.height + 30, self.view.frame.size.width - 32, 30)];
    howMuchLabel.text = @"How much do you spend on your habit?";
    howMuchLabel.font = [UIFont systemFontOfSize:15];
    howMuchLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:howMuchLabel];
    
    UIView *currencyToggleButtons = [[UIView alloc] initWithFrame:CGRectMake(howMuchLabel.frame.origin.x, howMuchLabel.frame.origin.y + howMuchLabel.frame.size.height + 12, howMuchLabel.frame.size.width, 40)];
    currencyToggleButtons.clipsToBounds = YES;
    [currencyToggleButtons setLayerEffect:1];
    currencyToggleButtons.layer.borderColor = [UIColor iQuitGrey1].CGColor;
    currencyToggleButtons.layer.borderWidth = 2;
    
    poundButton = [self createToggleButtonWithFrame:CGRectMake(0, 0, currencyToggleButtons.frame.size.width/2, currencyToggleButtons.frame.size.height) andSymbol:@"£"];
    poundButton.tag = 0;
    [poundButton addTarget:self action:@selector(currencyToggleSelected:) forControlEvents:UIControlEventTouchDown];
    
    dollarButton = [self createToggleButtonWithFrame:CGRectMake(poundButton.frame.size.width, 0, currencyToggleButtons.frame.size.width/2, currencyToggleButtons.frame.size.height) andSymbol:@"$"];
    dollarButton.tag = 1;
    [dollarButton addTarget:self action:@selector(currencyToggleSelected:) forControlEvents:UIControlEventTouchDown];
    
    [self setToggleSelected:0];
    
    [currencyToggleButtons addSubview:poundButton];
    [currencyToggleButtons addSubview:dollarButton];
    [self.view addSubview:currencyToggleButtons];
    
    moneyTextField = [[UITextField alloc] initWithFrame:CGRectMake(howMuchLabel.frame.origin.x, currencyToggleButtons.frame.origin.y + currencyToggleButtons.frame.size.height + 12, ((self.view.frame.size.width - (howMuchLabel.frame.origin.x * 2))/2) - 6, 40)];
    moneyTextField.backgroundColor = [UIColor iQuitGrey1];
    [moneyTextField setLayerEffect:1];
    moneyTextField.keyboardType = UIKeyboardTypeDecimalPad;
    [self.view addSubview:moneyTextField];
}

-(void)currencyToggleSelected:(id)sender
{
    [self setToggleSelected:(int)((UIButton*)sender).tag];
}

-(void)setToggleSelected:(int)toggle
{
    // 0 = Pounds, 1 = Dollars
    if(toggle == 0){
        [poundButton setBackgroundColor:[UIColor iQuitGrey1]];
        [dollarButton setBackgroundColor:[UIColor whiteColor]];
        [poundButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [dollarButton setTitleColor:[UIColor iQuitGrey1] forState:UIControlStateNormal];
    }
    else{
        [poundButton setBackgroundColor:[UIColor whiteColor]];
        [dollarButton setBackgroundColor:[UIColor iQuitGrey1]];
        [poundButton setTitleColor:[UIColor iQuitGrey1] forState:UIControlStateNormal];
        [dollarButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    currencySelected = toggle;
}

-(UIButton*)createToggleButtonWithFrame:(CGRect)frame andSymbol:(NSString*)symbol
{
    UIButton *button = [[UIButton alloc] initWithFrame:frame];
    [button setTitle:symbol forState:UIControlStateNormal];
    [button setBackgroundColor:[UIColor whiteColor]];
    [button setTitleColor:[UIColor iQuitGrey1] forState:UIControlStateNormal];
    return button;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
