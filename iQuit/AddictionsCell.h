//
//  AddictionsCell.h
//  iQuit
//
//  Created by Ryan Salton on 20/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddictionsCell : UICollectionViewCell

@property (nonatomic, strong) UILabel *cellTitle;
@property (nonatomic, strong) UIImageView *cellImage;

@end
