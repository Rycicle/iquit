//
//  ViewController.h
//  iQuit
//
//  Created by Ryan Salton on 20/05/2014.
//  Copyright (c) 2014 Ryan Salton. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AddictionsCollectionView.h"

@interface ViewController : BaseViewController <UICollectionViewDataSource, UICollectionViewDelegate>

@end
